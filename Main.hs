import Data.Char (toLower)
import Graphics.GD (Image, loadGifFile, loadJpegFile, loadPngFile, resizeImage, saveGifFile, saveJpegFile, savePngFile)
import System.Environment (getArgs)
import System.FilePath (takeExtension)

main :: IO ()
main = getArgs >>= \[source, width, height, destination] -> makeResize (read width, read height) source destination

makeResize :: (Int, Int) -> FilePath -> FilePath -> IO ()
makeResize dims source destination =
    do
        img <- loadImage source
        newImg <- uncurry resizeImage dims img
        saveImage destination newImg

loadImage :: FilePath -> IO Image
loadImage path =
    case lowercase $ takeExtension path of
        ".gif" -> loadGifFile path
        ".jpg" -> loadJpegFile path
        ".jpeg" -> loadJpegFile path
        ".png" -> loadPngFile path
        _ -> error "Unknown image extension"

saveImage :: FilePath -> Image -> IO ()
saveImage path img =
    case lowercase $ takeExtension path of
        ".gif" -> saveGifFile path img
        ".jpg" -> saveJpegFile 90 path img
        ".jpeg" -> saveJpegFile 90 path img
        ".png" -> savePngFile path img
        _ -> error "Unknown image extension"

lowercase :: String -> String
lowercase = map toLower

both :: (a -> b) -> (a, a) -> (b, b)
both f (a, b) = (f a, f b)
